using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour {
	
	// Public parameters
	public Vector3 startpoint;
	public Vector3 endpoint;
	public float speed = 1.0f;

	// Private state
	private bool movingForward = true;
	
	// Use this for initialization
	void Start () {
		transform.position = startpoint;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 target;
		
		if (movingForward) {
			target = endpoint;
		} else {
			target = startpoint;
		}

		float distanceToMove = speed * Time.deltaTime;
		
		Debug.Log ("distance to move = " + distanceToMove);

		float distanceToTarget = (target - transform.position).magnitude;
		
		Debug.Log ("distance to target = " + distanceToTarget);

		if (distanceToMove > distanceToTarget) {
			transform.position = target;
			movingForward = !movingForward;
		} else {
			Vector3 dir = (target - transform.position).normalized;
			transform.position += dir * distanceToMove;
		}
		
	}
}
